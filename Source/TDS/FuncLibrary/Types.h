// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Aim_State UMETA(DisplayName = "Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
	AimWalk_State UMETA(DisplayName = "AimWalk State"),
	SpeedRun_State UMETA(DisplayName = "SpeedRun State"),
	Sitting_State UMETA(DisplayName = "Sitting State"),
	Lie_State UMETA(DisplayName = "Lieng State")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	RifleType UMETA(DisplayName = "Rifle"),
	ShotGunType UMETA(DisplayName = "ShotGun"),
	PistolType UMETA(DisplayName = "Pistol"),
	SniperRifleType UMETA(DisplayName = "SniperRifle"),
	Grenate UMETA(DisplayName = "Granate"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeed = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float WalkSpeed = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float RunSpeed = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimWalkSpeed = 150.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SpeedRunSpeed = 800.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SittingSpeed = 100.0f; 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float LieSpeed = 0.0f;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	//StandartBullet
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		UStaticMesh* BulletMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		FTransform BulletMeshOffset  = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		UParticleSystem* BulletFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		FTransform BulletFXOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		TSubclassOf<class AProjectileClass> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		TMap<TEnumAsByte<EPhysicalSurface>,UMaterialInterface*> HitDecals;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		USoundBase* HitSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		FTransform HitFXsOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ProjectileDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings")
		float ProjectileSpeed = 2000.0f;


	//Granate and ExplosingBullet
	UPROPERTY()
		bool bIsLikeBomb = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings_Explosion")
		USoundBase* ExplosionSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings_Explosion")
		UParticleSystem* ExplosionFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings_Explosion")
		float ProjectileMaxRadiusDamage = 2000.0f; 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings_Explosion")
		float CoefDegreateDamage = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings_Explosion")
		float RadiusFullDamage = 1000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings_Explosion")
		float ExplosionDamage = 40.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSettings_Explosion")
		float TimeForExplose = 5.0f;
};


USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	//Dispersion_Aim
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Aim")
		float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Aim")
		float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Aim")
		float Aim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Aim")
		float Aim_StateDispersionAimReduction = 0.3f;

	//Dispersion_AimWalk
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_AimWalk")
		float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_AimWalk")
		float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_AimWalk")
		float AimWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_AimWalk")
		float AimWalk_StateDispersionAimReduction = 0.4f;

	//Dispersion_Walk
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Walk")
		float Walk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Walk")
		float Walk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Walk")
		float Walk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Walk")
		float Walk_StateDispersionAimReduction = 0.2f;

	//Dispersion_Idle
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Idle")
		float Idle_StateDispersionAimMax = 4.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Idle")
		float Idle_StateDispersionAimMin = 0.6f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Idle")
		float Idle_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Idle")
		float Idle_StateDispersionAimReduction = 0.15f;

	//Dispersion_Sit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Sit")
		float Sit_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Sit")
		float Sit_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Sit")
		float Sit_StateDispersionAimRecoil = 0.8f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Sit")
		float Sit_StateDispersionAimReduction = 0.4f;

	//Dispersion_Lie
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Lie")
		float Lie_StateDispersionAimMax = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Lie")
		float Lie_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Lie")
		float Lie_StateDispersionAimRecoil = 0.4f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion_Lie")
		float Lie_StateDispersionAimReduction = 0.4f;
};

USTRUCT(BlueprintType)
struct FArmySlot
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Army_Slot")
		int32 Count = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Army_Slot")
		int32 CurrentCount = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Army_Slot")
		int32 MaxCount = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Army_Slot")
		EWeaponType WeaponType = EWeaponType::PistolType;
};

USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon_Slot")
		FName NameItem;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon_Slot")
	//	FAddicionalWeaponInfo AddicionalWeaponInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon_Slot")
		FArmySlot Ammo;

};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	//Weapon and Bullets
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		TSubclassOf <class AWeaponClass> WeaponClass = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		FProjectileInfo ProjectileSettings;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		EWeaponType WeaponType = EWeaponType::PistolType;


	//MainSettings
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float RateOfReload = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		int32 MaxRound = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		int32 NumberProjectile = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float WeaponDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float AimRange = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		FWeaponDispersion  WeaponDispersion;


	//Tracing
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float DistanceTrace = 2000.0f;



	//FXEffects
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect")
		UDecalComponent* DecalOnHit = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireEffect")
		UParticleSystem* EffectFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		UAnimMontage* AnimAimCharFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		UAnimMontage* AnimCharReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundFinishReloadWeapon = nullptr;

	//StaticMeshes and AActors
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UStaticMesh* MagazineDrop = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UStaticMesh* NewMagazine = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UStaticMesh* EmptyBullet = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		TSubclassOf<AActor> SleeveBullets = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		TSubclassOf<AActor> DropMagaz = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		TSubclassOf<AActor> NewMagaz = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh")
		UTexture2D* WeaponIcon = nullptr;

};

//USTRUCT(BlueprintType)
//struct FAddicionalWeaponInfo
//{
//	GENERATED_BODY()
//
//	//CurrentCounts
//	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon_State")
//		int32 Round = 10;
//};





UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};