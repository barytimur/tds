// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponClass.h"
#include "DrawDebugHelpers.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Animation/AnimMontage.h"


// Sets default values
AWeaponClass::AWeaponClass()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponClass::BeginPlay()
{
	Super::BeginPlay();
	
	WeaponInit();
}

// Called every frame
void AWeaponClass::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);	
	DispersionTick(DeltaTime);
	ReloadTick(DeltaTime);
}

void AWeaponClass::FireTick(float DeltaTime)
{
	if (WeaponFiring)
	{
			if (CheckWeaponCanFire())
			{
				if (FireTime < 0.f)
				{
					Fire();
				}
			}
	}
	FireTime -= DeltaTime;
}

void AWeaponClass::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
			{
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			}
			else
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
			}
			if (CurrentDispersion < CurrentDispersionMin)
			{
				CurrentDispersion = CurrentDispersionMin;
			}
			else
			{
				if (CurrentDispersion > CurrentDispersionMax)
				{
					CurrentDispersion = CurrentDispersionMax;
				}
			}	
		}
	}
	/*if (DrawCone)
	{
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
	}*/
}

void AWeaponClass::ReloadTick(float DeltaTime)
{
	if (GetWeaponRound() == 0)
	{
		ReloadingEvent.Broadcast();
	}
	if (WeaponReloading)
	{
		if (ReloadTime < 0)
		{
			FinishReload();
		}
		ReloadTime -= DeltaTime;
	}
}

void AWeaponClass::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
	UpdateStateWeapon(EMovementState::Run_State);
}

FProjectileInfo AWeaponClass::GetProjectile()
{
	return WeaponSetting.ProjectileSettings;
}

int8 AWeaponClass::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectile;
}

void AWeaponClass::UpdateStateWeapon(EMovementState NewMovementState)
{
	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		CurrentDispersionMax = WeaponSetting.WeaponDispersion.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.WeaponDispersion.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.WeaponDispersion.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.WeaponDispersion.Aim_StateDispersionAimReduction;
		break;
	case EMovementState::Walk_State:
		CurrentDispersionMax = WeaponSetting.WeaponDispersion.Walk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.WeaponDispersion.Walk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.WeaponDispersion.Walk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.WeaponDispersion.Walk_StateDispersionAimReduction;
		break;
	case EMovementState::Run_State:
		CurrentDispersionMax = WeaponSetting.WeaponDispersion.Idle_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.WeaponDispersion.Idle_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.WeaponDispersion.Idle_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.WeaponDispersion.Idle_StateDispersionAimReduction;
		break;
	case EMovementState::AimWalk_State:
		CurrentDispersionMax = WeaponSetting.WeaponDispersion.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.WeaponDispersion.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.WeaponDispersion.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.WeaponDispersion.AimWalk_StateDispersionAimReduction;
		break;
	case EMovementState::SpeedRun_State:
		break;
	case EMovementState::Sitting_State:
		CurrentDispersionMax = WeaponSetting.WeaponDispersion.Sit_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.WeaponDispersion.Sit_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.WeaponDispersion.Sit_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.WeaponDispersion.Sit_StateDispersionAimReduction;
		break;
	case EMovementState::Lie_State:
		CurrentDispersionMax = WeaponSetting.WeaponDispersion.Lie_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.WeaponDispersion.Lie_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.WeaponDispersion.Lie_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.WeaponDispersion.Lie_StateDispersionAimReduction;
		break;
	default:
		break;
	}
}

void AWeaponClass::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
	{
		WeaponFiring = bIsFire;
	}
	else
	{
		WeaponFiring = false;
		FireTime = 0.1f;
	}
}

bool AWeaponClass::CheckWeaponCanFire()
{
	if (GetWeaponRound() > 0)
	{
		if (WeaponReloading)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	else
	{
		return false;
	}

}

void AWeaponClass::Fire()
{
	FireTime = WeaponSetting.RateOfFire;
	int countProject = 1;
	ChangeDispersionByShoot();
	// ShootLocation->GetComponentTransform());
	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
		FTransform EmitterTransf(ShootLocation->GetComponentRotation(), ShootLocation->GetComponentLocation(), FVector(0.1f, 0.1f, 0.1f));
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, EmitterTransf);
		if (WeaponInfo.Ammo.Count < WeaponSetting.NumberProjectile)
		{
			countProject = WeaponInfo.Ammo.Count;
		}
		else
		{
			countProject = WeaponSetting.NumberProjectile;
		}
		for (int i = 0; i < countProject; i++)
		{	
			WeaponInfo.Ammo.Count--;
			FVector EndLocation;
			EndLocation = GetFireEndLocation();
			FVector Dir = EndLocation - SpawnLocation;
			Dir.Normalize();
			FMatrix MyMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector::ZeroVector);
			SpawnRotation = MyMatrix.Rotator();

			if (ProjectileInfo.Projectile)
			{
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();
				AProjectileClass* myProjectile = Cast<AProjectileClass>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile->ProjectileSetting.bIsLikeBomb)
				{			
					if (myProjectile)
					{
						//ToDo Init Projectile settings by id in table row(or keep in weapon table)
						myProjectile->InitProjectile(WeaponSetting.ProjectileSettings);
					}
				}
				else
				{
					if (myProjectile)
					{
						//ToDo Init Projectile settings by id in table row(or keep in weapon table)
						myProjectile->InitProjectile(WeaponSetting.ProjectileSettings);
					}
				}
			}
			else
			{
				FHitResult hit;
				GetWorld()->LineTraceSingleByChannel(hit, SpawnLocation, EndLocation, ECollisionChannel::ECC_WorldStatic );
				DrawDebugLine(GetWorld(), SpawnLocation, hit.Location, FColor::Black, false, 5.0f, (uint8)'\000', 0.5f);
				//ToDo Projectile null Init trace fire			
			}
		}
		ShootingEvent.Broadcast();	
	}
}

void AWeaponClass::Droping()
{
	FVector SpawnLocation = ShootLocation->GetComponentLocation();
	FRotator SpawnRotation = ShootLocation->GetComponentRotation();
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();
	FProjectileInfo ProjectileInfo;
	ProjectileInfo = GetProjectile();
	if (ProjectileInfo.Projectile)
	{
		if (WeaponSetting.SleeveBullets)
		{
			if (Anim)
			{
				SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(Anim,0.9f);
			}
			AActor* DropBullet = GetWorld()->SpawnActor(WeaponSetting.SleeveBullets, &SpawnLocation, &SpawnRotation, SpawnParams);
			DropBullet->SetLifeSpan(5.0f);
			UStaticMeshComponent* dropmesh = Cast<UStaticMeshComponent>(DropBullet->GetComponentByClass(UStaticMeshComponent::StaticClass()));
			dropmesh->SetStaticMesh(WeaponSetting.EmptyBullet);
			FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
			dropmesh->AttachToComponent(SkeletalMeshWeapon, Rule, FName("BulletDrop"));
			dropmesh->SetSimulatePhysics(true);
			dropmesh->AddImpulse(ShootLocation->GetForwardVector() * FVector(-5, -20, 10));
		}
	}
}


float AWeaponClass::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponClass::ApplyDispersionToShoot(FVector DirectionShoot) const
{

	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

void AWeaponClass::ChangeDispersionByShoot()
{
	CurrentDispersion = CurrentDispersion = CurrentDispersionRecoil;
}

FVector AWeaponClass::GetFireEndLocation() const
{
	bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);
	EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;	
	////Debug Delete
	//if (DrawCone)
	//{
	//	DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistanceTrace, GetCurrentDispersion() * PI / 180.0f, GetCurrentDispersion() * PI / 180.0f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	//	DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Red, false, 5.0f, (uint8)'\000', 0.5f);
	//	DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.0f, (uint8)'\000', 0.5f);
	//}
	////////////////////////////////////////
	return EndLocation;
}

void AWeaponClass::Reload()
{
	if (WeaponInfo.Ammo.CurrentCount != 0)
	{
		WeaponReloading = true;
		UGameplayStatics::PlaySound2D(GetWorld(), WeaponSetting.SoundReloadWeapon);
		ReloadTime = WeaponSetting.RateOfReload;
	}
}

int32 AWeaponClass::GetWeaponRound()
{
	return WeaponInfo.Ammo.Count;
}


void AWeaponClass::FinishReload()
{
	WeaponReloading = false;
	UGameplayStatics::PlaySound2D(GetWorld(), WeaponSetting.SoundFinishReloadWeapon);
	if (WeaponInfo.Ammo.MaxCount != 0)
	{
		if (WeaponInfo.Ammo.CurrentCount + WeaponInfo.Ammo.Count < WeaponSetting.MaxRound)
		{
			WeaponInfo.Ammo.Count += WeaponInfo.Ammo.CurrentCount;
			WeaponInfo.Ammo.CurrentCount = 0;
		}
		else
		{
			int temp = WeaponInfo.Ammo.Count;
			WeaponInfo.Ammo.Count = WeaponSetting.MaxRound;
			WeaponInfo.Ammo.CurrentCount -= WeaponSetting.MaxRound;
			WeaponInfo.Ammo.CurrentCount += temp;
		}
	}
	else
	{
		WeaponInfo.Ammo.Count = WeaponSetting.MaxRound;
	}
	FinishReloadingEvent.Broadcast();
}

void AWeaponClass::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
	{
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);
	}
}

