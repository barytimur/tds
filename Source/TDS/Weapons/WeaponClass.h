// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "TDS/FuncLibrary/Types.h"
#include "TDS/Weapons/Projectiles/ProjectileClass.h"
#include "WeaponClass.generated.h"


//DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponFireStart);//ToDo Delegate on event weapon fire - Anim char, state char...
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FReloadingEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FShootingEvent);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FFinishReloadingEvent);

UCLASS()
class TDS_API AWeaponClass : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponClass();

	//Delegates
	FReloadingEvent ReloadingEvent;
	FShootingEvent ShootingEvent;
	FFinishReloadingEvent FinishReloadingEvent;

	//MainComponents
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;

	//WeaponSettings
	UPROPERTY()
	FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FWeaponSlot WeaponInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	UAnimMontage* Anim;
	class UTDSInventoryComponent* InventoryWeaponComponent;

	//FireSettings
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponReloading = false;
	float FireTime = 0.0f;
	float ReloadTime = 0.0f;

	//Dispersion
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;
	bool ShouldReduceDispersion = false;

	//WeaponNameInTable
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Name")
	EWeaponType NameInTable;

	//DebugDelete
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
	bool DrawCone = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void DispersionTick(float DeltaTime);
	void ReloadTick(float DeltaTime);

	//WeaponSettings
	UFUNCTION()
	void WeaponInit();
	UFUNCTION()
	FProjectileInfo GetProjectile();
	//Shotgun
	int8 GetNumberProjectileByShot()const;

	//Fire
	UFUNCTION()
	void UpdateStateWeapon(EMovementState NewMovementState);
	UFUNCTION()
	void SetWeaponStateFire(bool bIsFire);
	UFUNCTION()
	bool CheckWeaponCanFire();
	UFUNCTION()
	void Fire();
	UFUNCTION()
	void Droping();

	//Dispersion
	UFUNCTION()
	float GetCurrentDispersion() const;
	UFUNCTION()
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
	UFUNCTION()
	void ChangeDispersionByShoot();
	UFUNCTION()
	FVector GetFireEndLocation() const;

	//Reload
	UFUNCTION()
	void Reload();
	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();
	UFUNCTION()
	void FinishReload();
	UFUNCTION()
	void CancelReload();
};
