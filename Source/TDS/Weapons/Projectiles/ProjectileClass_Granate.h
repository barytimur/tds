// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "TDS/Weapons/Projectiles/ProjectileClass.h"
#include "ProjectileClass_Granate.generated.h"

/**
 * 
 */

UCLASS()
class TDS_API AProjectileClass_Granate : public AProjectileClass
{
	GENERATED_BODY()

public:
	bool TimerEnabled = false;
	float TimerToExplose = 0.0f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void TimerExplose(float DeltaTime);
	UFUNCTION()
	void Explose();

	virtual void ImpactProjectile() override;
	virtual void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

};
