// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileClass_Granate.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

void AProjectileClass_Granate::BeginPlay()
{
	Super::BeginPlay();
	ProjectileSetting.bIsLikeBomb = true;
}
void AProjectileClass_Granate::Tick(float DeltaTime)
{
	TimerExplose(DeltaTime);
}

void AProjectileClass_Granate::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > ProjectileSetting.TimeForExplose)
		{
			Explose();
		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileClass_Granate::Explose()
{
	TimerEnabled = false;
	if (ProjectileSetting.ExplosionFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExplosionFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExplosionSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExplosionSound, GetActorLocation());
	}
	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExplosionDamage,
		ProjectileSetting.ExplosionDamage * 0.2f,
		GetActorLocation(),
		ProjectileSetting.RadiusFullDamage,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		ProjectileSetting.CoefDegreateDamage,
		NULL, IgnoredActor, nullptr, nullptr);
	this->Destroy();
}

void AProjectileClass_Granate::ImpactProjectile()
{
	TimerEnabled = true;
}

void AProjectileClass_Granate::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}