// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSInventoryComponent.h"

// Sets default values for this component's properties
UTDSInventoryComponent::UTDSInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...
}


// Called when the game starts
void UTDSInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	//MaxSlots = WeaponSlots.Num();
	MaxSlots = 4;
	for (int i = WeaponSlots.Num(); i >= 0; i--)
	{
		if (i > MaxSlots)
		{
			WeaponSlots.Pop();
		}
	}
	for (int i = 0; i < WeaponSlots.Num(); i++)
	{
		if (!WeaponSlots[i].NameItem.IsNone())
		{
			if (GetWeaponIndexSlotByName(WeaponSlots[i].NameItem) != i)
			{
				WeaponSlots[i].NameItem = "None";
				SetAdditionalWeaponInfo(i, WeaponSlots[i]);
			}
		}
	}
	for (int i = 0; i < WeaponSlots.Num(); i++)
	{
		if (!WeaponSlots[i].NameItem.IsNone())
		{
			CreateSlot.Broadcast(WeaponSlots[i]);
			OnUpdateWidget.Broadcast(WeaponSlots[i], false);
		}
	}
	
	if (WeaponSlots.IsValidIndex(1))
	{
		if (!WeaponSlots[1].NameItem.IsNone())
		{
			OnSwitchWeapon.Broadcast(WeaponSlots[1].NameItem, WeaponSlots[1],true,false);
		}
	}
	
}


// Called every frame
void UTDSInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UTDSInventoryComponent::SwitchWeaonToIndex(int32 NewIndex, int32 oldIndex, FWeaponSlot oldInfo, bool bIsNext, bool bIsGranate)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = NewIndex;
	if (CorrectIndex > WeaponSlots.Num() - 1)
	{
		CorrectIndex = 0;
	}
	else
	{
		if (CorrectIndex < 0)
		{
			CorrectIndex = WeaponSlots.Num() - 1;
		}
	}

	FName NewIdWeapon;
	FWeaponSlot NewAdditionalInfo;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsSuccess)
	{
		if (i == CorrectIndex)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				if (!bIsGranate)
				{
					if (WeaponSlots[i].Ammo.WeaponType != EWeaponType::Grenate)
					{
						NewIdWeapon = WeaponSlots[i].NameItem;
						NewAdditionalInfo = WeaponSlots[i];
						bIsSuccess = true;
					}		
				}
				else
				{
					NewIdWeapon = WeaponSlots[i].NameItem;
					NewAdditionalInfo = WeaponSlots[i];
					bIsSuccess = true;
				}				
			}
		}
		i++;
	}
	if (!bIsSuccess)
	{
		if (bIsNext)
		{
			SwitchWeaonToIndex(CorrectIndex + 1, CorrectIndex, WeaponSlots[CorrectIndex],true,false);
		}
		else
		{
			SwitchWeaonToIndex(CorrectIndex - 1, CorrectIndex, WeaponSlots[CorrectIndex],false,false);
		}	
	}
	if (bIsSuccess)
	{
		SetAdditionalWeaponInfo(oldIndex, oldInfo);
		if (GetWeaponIndexSlotByName(NewIdWeapon) > 2)
		{
			OnSwitchWhenSwap.Broadcast(NewIdWeapon);
		}
		else
		{
			OnSwitchWhenSwap.Broadcast(WeaponSlots[2].NameItem);
		}	
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo,true, false);
	}

	return bIsSuccess;
}

FWeaponSlot UTDSInventoryComponent::GetAddicionalWeaponInfo(int32 indexWeapon)
{
	return FWeaponSlot();
}

int32 UTDSInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	bool bIsSuccess = false;
	int32 CorrectIndex;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsSuccess)
	{
		if (WeaponSlots[i].NameItem.IsEqual(IdWeaponName))
		{
			CorrectIndex = i;
			bIsSuccess = true;
		}
		i++;
	}
	return CorrectIndex;
}

FName UTDSInventoryComponent::GetWeaponNameSlotByType(EWeaponType IdWeaponType)
{
	bool bIsSuccess = false;
	FName CorrectName;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsSuccess)
	{
		if (WeaponSlots[i].Ammo.WeaponType == IdWeaponType)
		{
			CorrectName = WeaponSlots[i].NameItem;
			bIsSuccess = true;
		}
		i++;
	}
	return CorrectName;
}

void UTDSInventoryComponent::SetAdditionalWeaponInfo(int32 IndexWeapon, FWeaponSlot NewInfo)
{
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (i == IndexWeapon)
			{
				WeaponSlots[i] = NewInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
		{
			UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::SetAdditionalWeaponInfo - Weapon not found with index"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UTDSInventoryComponent::SetAdditionalWeaponInfo - Uncorrect weapon index"));
	}
}

bool UTDSInventoryComponent::AddCurrentBullets(EWeaponType WeaponType, int32 CounterBullets, EWeaponType CurWeaponType)
{
	int32 CurWeap = GetWeaponIndexSlotByName(GetWeaponNameSlotByType(WeaponType));
	int32 CurWeapinHand = GetWeaponIndexSlotByName(GetWeaponNameSlotByType(CurWeaponType));
	bool maxammo = false;
	if (CurWeap == CurWeapinHand)
	{
		if (WeaponSlots[CurWeapinHand].Ammo.CurrentCount + CounterBullets > WeaponSlots[CurWeapinHand].Ammo.MaxCount)
		{
			WeaponSlots[CurWeapinHand].Ammo.CurrentCount = WeaponSlots[CurWeapinHand].Ammo.MaxCount;
			maxammo = true;
		}
		else
		{
			WeaponSlots[CurWeapinHand].Ammo.CurrentCount += CounterBullets;
		}
		//SetAdditionalWeaponInfo(CurWeapinHand, WeaponSlots[CurWeapinHand]);
		SwitchWeaonToIndex(1, CurWeapinHand, WeaponSlots[CurWeapinHand],true,false);
		SwitchWeaonToIndex(CurWeapinHand, 1, WeaponSlots[1],true,false);
		OnUpdateWidget.Broadcast(WeaponSlots[CurWeapinHand], false);
	}
	else
	{
		if (WeaponSlots[CurWeap].Ammo.CurrentCount + CounterBullets > WeaponSlots[CurWeap].Ammo.MaxCount)
		{
			WeaponSlots[CurWeap].Ammo.CurrentCount = WeaponSlots[CurWeap].Ammo.MaxCount;
			maxammo = true;
		}
		else
		{
			WeaponSlots[CurWeap].Ammo.CurrentCount += CounterBullets;
		}
		SetAdditionalWeaponInfo(CurWeap, WeaponSlots[CurWeap]);
		OnUpdateWidget.Broadcast(WeaponSlots[CurWeap], false);
	}
	
	return maxammo;
	
}

int32 UTDSInventoryComponent::CheckSlots()
{
	bool bIsFind = false;
	int32 result = -1;
	int8 i = 0;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem.IsNone())
		{
			bIsFind = true;
			result = i;
		}
		i++;
	}
	return result;
}

bool UTDSInventoryComponent::PickUpWeapon(FWeaponSlot WeaponSlot)
{
	bool bIsSuccess = false;
	int32 index = CheckSlots();
	if (GetWeaponNameSlotByType(WeaponSlot.Ammo.WeaponType).IsNone())
	{
		if (WeaponSlots.IsValidIndex(index))
		{
			SetAdditionalWeaponInfo(index, WeaponSlot);
			//WeaponSlots[index] = WeaponSlot;
			OnSwitchWeapon.Broadcast(WeaponSlots[index].NameItem, WeaponSlot, false, false);
			bIsSuccess = true;
		}
	}
	return bIsSuccess;
}

FWeaponSlot UTDSInventoryComponent::SwapWeapon(FWeaponSlot NewWeaponSlot, FWeaponSlot CurrentWeaponSlot)
{
	int32 index = GetWeaponIndexSlotByName(CurrentWeaponSlot.NameItem);
	if (index < 2)
	{
		index = 2;
		CurrentWeaponSlot = WeaponSlots[index];
	}
	if (GetWeaponNameSlotByType(NewWeaponSlot.Ammo.WeaponType).IsNone())
	{
			SetAdditionalWeaponInfo(index, NewWeaponSlot);
			//WeaponSlots[index] = WeaponSlot;
			OnSwitchWeapon.Broadcast(WeaponSlots[index].NameItem, NewWeaponSlot, true,true);
	}
	return CurrentWeaponSlot;
}
