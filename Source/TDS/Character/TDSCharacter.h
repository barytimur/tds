// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "TDS/Character/TDSInventoryComponent.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDS/Weapons/WeaponClass.h"
#include "TDSCharacter.generated.h"

class UAnimMontage;


UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
	GENERATED_BODY()
protected:
	virtual void BeginPlay() override;

public:
	ATDSCharacter();

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UTDSInventoryComponent* InventoryComponent;

	//Movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool WalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SpeedRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool AimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool SittingEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool LieingEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool LeftButton = false;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Movement")
		float RotateStep = 350.0f;
	float AxisX = 0.0f;
	float AxisY = 0.0f;
	

	//Animation
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Animation")
		UAnimMontage* StandToCrouchMontage;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Animation")
		UAnimMontage* CrouchToStanMontage;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Animation")
		UAnimMontage* ProneToStandMontage;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Animation")
		UAnimMontage* StandToProneCrouchMontage;

	//Sound
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundJump = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* Music = nullptr;

	//Weapon
	UPROPERTY(BlueprintReadWrite)
	AWeaponClass* CurrentWeapon = nullptr;

	FName StandartWeaponName;
	UPROPERTY(BlueprintReadOnly)
	bool pistol = false;
	AActor* dropmag = nullptr;
	AActor* newmag = nullptr;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;


public:

	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;


	
	//Movement
	UFUNCTION()
	void MovementTick(float DeltaTime);
	UFUNCTION()
	void InputAxisX(float Value);
	UFUNCTION()
	void InputAxisY(float Value);
	UFUNCTION()
	void ChangeRotationInMovement();
	UFUNCTION()
	void ChangeRotation(float DeltaTime);
	UFUNCTION()
	void ChangeMovementState();
	UFUNCTION()
	void CharacterUpdate();


	//Weapon
	UFUNCTION(BlueprintCallable)
	void InitWeapon(FName IdWeapon, FWeaponSlot Bullets, bool bIsOld, bool bIsSwap);
	UFUNCTION()
	void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
	AWeaponClass* GetCurrentWeapon();
	UFUNCTION()
	void ReloadWeapon();
	UFUNCTION()
	void FinReloadWeapon();
	UFUNCTION()
	void SwitchWeaponToNext();
	UFUNCTION()
	void SwitchWeaponToPre();


	//Bind Buttons
	UFUNCTION()
	void Sprinting();
	UFUNCTION()
	void Sprinting_Done();
	UFUNCTION()
	void Walking();
	UFUNCTION()
	void Walking_Done();
	UFUNCTION()
	void Aiming();
	UFUNCTION()
	void Aiming_Done();
	UFUNCTION()
	void LeftCLicked();
	UFUNCTION()
	void LeftReleased();
	UFUNCTION()
	void Sitting();
	UFUNCTION()
	void Liing();
	UFUNCTION()
	void Jumping();
	UFUNCTION()
	void InitGranate();
	UFUNCTION()
	void ThrowGran();

	//Animation
	UFUNCTION()
	void ShootMontage();
	UFUNCTION()
	void OnCompletedMontage(UAnimMontage* aMontage, bool bInterrupted);


};

