// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Components/SceneComponent.h"
#include "Animation/AnimMontage.h"
#include "TDS/Game/TDSGameInstance.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprints/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());


	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("InventoryComponent"));
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDSCharacter::InitWeapon);
	}
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSCharacter::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::PlaySound2D(GetWorld(), Music);
	GetMesh()->GetAnimInstance()->OnMontageEnded.AddDynamic(this, &ATDSCharacter::OnCompletedMontage);
}

void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* InputComponents)
{
	Super::SetupPlayerInputComponent(InputComponents);

	InputComponents->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	InputComponents->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);

	InputComponents->BindAction(TEXT("LMB"), EInputEvent::IE_Pressed, this, &ATDSCharacter::LeftCLicked);
	InputComponents->BindAction(TEXT("LMB"), EInputEvent::IE_Released, this, &ATDSCharacter::LeftReleased);

	InputComponents->BindAction(TEXT("Walking"), EInputEvent::IE_Pressed, this, &ATDSCharacter::Walking);
	InputComponents->BindAction(TEXT("Walking"), EInputEvent::IE_Released, this, &ATDSCharacter::Walking_Done);

	InputComponents->BindAction(TEXT("Sprint"), EInputEvent::IE_Pressed, this, &ATDSCharacter::Sprinting);
	InputComponents->BindAction(TEXT("Sprint"), EInputEvent::IE_Released, this, &ATDSCharacter::Sprinting_Done);

	InputComponents->BindAction(TEXT("RMB"), EInputEvent::IE_Pressed, this, &ATDSCharacter::Aiming);
	InputComponents->BindAction(TEXT("RMB"), EInputEvent::IE_Released, this, &ATDSCharacter::Aiming_Done);

	InputComponents->BindAction(TEXT("ThrowGranate"), EInputEvent::IE_Pressed, this, &ATDSCharacter::InitGranate);
	InputComponents->BindAction(TEXT("ThrowGranate"), EInputEvent::IE_Released, this, &ATDSCharacter::ThrowGran);

	InputComponents->BindAction(TEXT("Sit"), EInputEvent::IE_Pressed, this, &ATDSCharacter::Sitting);

	InputComponents->BindAction(TEXT("Lie"), EInputEvent::IE_Pressed, this, &ATDSCharacter::Liing);

	InputComponents->BindAction(TEXT("Jumping"), EInputEvent::IE_Pressed, this, &ATDSCharacter::Jumping);

	InputComponents->BindAction(TEXT("ReloadingBut"), EInputEvent::IE_Pressed, this, &ATDSCharacter::ReloadWeapon);

	InputComponents->BindAction(TEXT("PreWeapon"), EInputEvent::IE_Released, this, &ATDSCharacter::SwitchWeaponToPre);
	InputComponents->BindAction(TEXT("NextWeapon"), EInputEvent::IE_Released, this, &ATDSCharacter::SwitchWeaponToNext);
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_GameTraceChannel2, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}
	MovementTick(DeltaSeconds);
}

void ATDSCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
	AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);
	APlayerController* PC = Cast<APlayerController>(GetController());
	FHitResult TraceHitResult;
	PC->GetHitResultUnderCursor(ECC_GameTraceChannel2, true, TraceHitResult);
	if (((AimEnabled || LeftButton)) && ((AxisX != 0) || (AxisY != 0)) && ((LieingEnabled == false)))
	{
		ChangeRotationInMovement();
	}
	if (((AimEnabled || LeftButton)) && ((AxisX == 0) && (AxisY == 0)))
	{
		ChangeRotation(DeltaTime);
	}
	if (SittingEnabled || LieingEnabled)
	{
		AimEnabled = true;
	}
	if (CurrentWeapon)
	{
		if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
		{
			CurrentWeapon->ShouldReduceDispersion = true;
		}
		else
		{
			CurrentWeapon->ShouldReduceDispersion = false;
		}
	}
	if (!SpeedRunEnabled)
	{
		if (((!WalkEnabled && !SpeedRunEnabled && !AimEnabled && !SittingEnabled && !LieingEnabled) && ((AxisX != 0) || (AxisY != 0))))
		{
			AttackCharEvent(false);
		}
	}
	else
	{
		AttackCharEvent(false);
	}
}


void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSCharacter::ChangeRotationInMovement()
{
	FVector AActorVec = GetActorLocation();
	FVector ACursorVec = CursorToWorld->GetComponentLocation();
	FRotator myRotate = UKismetMathLibrary::FindLookAtRotation(GetActorForwardVector(), (ACursorVec - AActorVec));
	SetActorRotation(FQuat(FRotator(0.0f, myRotate.Yaw, 0.0f)));
}

void ATDSCharacter::ChangeRotation(float DeltaTime)
{
	FVector AActorVec = GetActorLocation();
	FVector ACursorVec = CursorToWorld->GetComponentLocation();
	FRotator myRotate = UKismetMathLibrary::FindLookAtRotation(GetActorForwardVector(), (ACursorVec - AActorVec));
	FRotator ActorRotate = GetActorRotation();
	int Rotate_Curs = round(myRotate.Yaw);
	int Rotate_Actor = round(ActorRotate.Yaw);
	if (UKismetMathLibrary::Clamp(Rotate_Curs, Rotate_Actor - 1, Rotate_Actor + 1) == Rotate_Curs)
	{
		SetActorRotation(FQuat(FRotator(0.0f, myRotate.Yaw, 0.0f)));
	}
	else
	{
		if (ActorRotate.Yaw > myRotate.Yaw)
		{
			if ((ActorRotate.Yaw - myRotate.Yaw) < 180)
			{
				FRotator NewRotation = GetActorRotation() + (FRotator(0, ((RotateStep * (-1)) * DeltaTime), 0));
				SetActorRotation(NewRotation);
			}
			else
			{
				FRotator NewRotation = GetActorRotation() + (FRotator(0, (RotateStep * DeltaTime), 0));
				SetActorRotation(NewRotation);
			}
		}
		else
		{
			if ((ActorRotate.Yaw - myRotate.Yaw) > (-180))
			{
				FRotator NewRotation = GetActorRotation() + (FRotator(0, (RotateStep * DeltaTime), 0));
				SetActorRotation(NewRotation);
			}
			else
			{
				FRotator NewRotation = GetActorRotation() + (FRotator(0, ((RotateStep * (-1)) * DeltaTime), 0));
				SetActorRotation(NewRotation);
			}
		}
	}
}

void ATDSCharacter::ChangeMovementState()
{
	if (!WalkEnabled && !SpeedRunEnabled && !AimEnabled && !SittingEnabled && !LieingEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SpeedRunEnabled)
		{
			LeftButton = false;
			WalkEnabled = false;
			AimEnabled = false;
			SittingEnabled = false;
			MovementState = EMovementState::SpeedRun_State;
		}
		if (WalkEnabled && !SpeedRunEnabled && !AimEnabled && !SittingEnabled && !LieingEnabled)
		{
			MovementState = EMovementState::Walk_State;
		}
		else
		{
			if (!WalkEnabled && !SpeedRunEnabled && AimEnabled && !SittingEnabled && !LieingEnabled)
			{
				MovementState = EMovementState::Aim_State;
			}
			else
			{
				if (WalkEnabled && !SpeedRunEnabled && AimEnabled && !SittingEnabled && !LieingEnabled)
				{
					MovementState = EMovementState::AimWalk_State;
				}
				else
				{
					if ((!WalkEnabled && !SpeedRunEnabled && !AimEnabled && SittingEnabled && !LieingEnabled) || (!WalkEnabled && !SpeedRunEnabled && AimEnabled && SittingEnabled && !LieingEnabled))
					{
						AimEnabled = true;
						MovementState = EMovementState::Sitting_State;
					}
					else
					{
						if ((!WalkEnabled && !SpeedRunEnabled && !AimEnabled && !SittingEnabled && LieingEnabled) || (!WalkEnabled && !SpeedRunEnabled && AimEnabled && !SittingEnabled && LieingEnabled))
						{
							AimEnabled = true;
							MovementState = EMovementState::Lie_State;
						}
					}
				}
			}
		}
	}
	CharacterUpdate();

	//Weapon state update
	AWeaponClass* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

void ATDSCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::SpeedRun_State:
		ResSpeed = MovementInfo.SpeedRunSpeed;
		break;
	case EMovementState::Sitting_State:
		ResSpeed = MovementInfo.SittingSpeed;
		break;
	case EMovementState::Lie_State:
		ResSpeed = MovementInfo.LieSpeed;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSCharacter::InitWeapon(FName IdWeapon, FWeaponSlot Bullets,bool bIsOld, bool bIsSwap)//ToDo Init by id row by table
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeapon, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponClass* myWeapon = Cast<AWeaponClass>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;
					if (myWeapon->NameInTable == InventoryComponent->WeaponSlots[1].Ammo.WeaponType)
					{
						pistol = true;
					}
					else
					{
						pistol = false;
					}
					myWeapon->WeaponSetting = myWeaponInfo;
					myWeapon->UpdateStateWeapon(MovementState);
					if (Bullets.Ammo.Count > myWeapon->WeaponSetting.MaxRound)
					{
						myWeapon->WeaponInfo = Bullets;
						myWeapon->WeaponInfo.Ammo.Count = myWeapon->WeaponSetting.MaxRound;
					}
					else
					{
						myWeapon->WeaponInfo = Bullets;
					}
					if (InventoryComponent)
					{
						//CurrentIndexWeapon = InventoryComponent->GetWeaponIndexSlotByName(IdWeapon);
					}
					myWeapon->ReloadingEvent.AddDynamic(this, &ATDSCharacter::ReloadWeapon);
					myWeapon->ShootingEvent.AddDynamic(this, &ATDSCharacter::ShootMontage);
					myWeapon->FinishReloadingEvent.AddDynamic(this, &ATDSCharacter::FinReloadWeapon);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::InitWeapon - Weapon not found"));
		}
		if (!bIsOld)
		{
			InventoryComponent->CreateSlot.Broadcast(Bullets);
		}
		if (bIsSwap)
		{
			int index = InventoryComponent->GetWeaponIndexSlotByName(Bullets.NameItem);
			InventoryComponent->OnSwapWeapon.Broadcast(CurrentWeapon->WeaponSetting, index,Bullets);
		}
		InventoryComponent->OnUpdateWidget.Broadcast(CurrentWeapon->WeaponInfo,true);
	}

}

void ATDSCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponClass* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		//ToDo Check melee or range
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATPSCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}
}



AWeaponClass* ATDSCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDSCharacter::ReloadWeapon()
{
	if (CurrentWeapon)
	{
		if (!CurrentWeapon->WeaponReloading)
		{
			if (CurrentWeapon->WeaponInfo.Ammo.Count < CurrentWeapon->WeaponSetting.MaxRound)
			{
				if (CurrentWeapon->WeaponInfo.Ammo.CurrentCount != 0)
				{
					CurrentWeapon->Reload();
					if ((!LieingEnabled) && (!SittingEnabled))
					{
						PlayAnimMontage(CurrentWeapon->WeaponSetting.AnimCharReload);
						if (CurrentWeapon->WeaponSetting.DropMagaz)
						{
							dropmag = GetWorld()->SpawnActor(CurrentWeapon->WeaponSetting.DropMagaz);
							dropmag->SetLifeSpan(15.0f);
							UStaticMeshComponent* dropmagazine = Cast<UStaticMeshComponent>(dropmag->GetComponentByClass(UStaticMeshComponent::StaticClass()));
							dropmagazine->SetStaticMesh(CurrentWeapon->WeaponSetting.MagazineDrop);
							FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
							dropmagazine->AttachToComponent(GetMesh(), Rule, FName("MagazineSocket"));
							dropmagazine->SetSimulatePhysics(true);
							newmag = GetWorld()->SpawnActor(CurrentWeapon->WeaponSetting.NewMagaz);
							newmag->SetLifeSpan(15.0f);
							UStaticMeshComponent* newmagazine = Cast<UStaticMeshComponent>(newmag->GetComponentByClass(UStaticMeshComponent::StaticClass()));
							newmagazine->SetStaticMesh(CurrentWeapon->WeaponSetting.NewMagazine);
							newmagazine->AttachToComponent(GetMesh(), Rule, FName("MagazineSocket"));
						}
					}
				}
			}
		}
		
	}
}
void ATDSCharacter::FinReloadWeapon()
{
	InventoryComponent->SetAdditionalWeaponInfo(InventoryComponent->GetWeaponIndexSlotByName(CurrentWeapon->WeaponInfo.NameItem), CurrentWeapon->WeaponInfo);
	InventoryComponent->OnUpdateWidget.Broadcast(CurrentWeapon->WeaponInfo,false);
}

void ATDSCharacter::SwitchWeaponToNext()
{
	FWeaponSlot OldInfo;
	int32 ind;
	int32 newind;
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			ind  = InventoryComponent->GetWeaponIndexSlotByName(FName(CurrentWeapon->WeaponInfo.NameItem));
			newind = ind + 1;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
				GetMesh()->GetAnimInstance()->StopAllMontages(0.015f);
				if (CurrentWeapon->WeaponSetting.DropMagaz)
				{
					dropmag->Destroy();
				}
				if (CurrentWeapon->WeaponSetting.NewMagaz)
				{
					newmag->Destroy();
				}
			}
		}
		if (InventoryComponent)
		{
			InventoryComponent->SwitchWeaonToIndex(newind, ind, OldInfo,true,false);
		}
	}
}

void ATDSCharacter::SwitchWeaponToPre()
{
	FWeaponSlot OldInfo;
	int32 ind;
	int32 newind;

	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->WeaponInfo;
			ind = InventoryComponent->GetWeaponIndexSlotByName(FName(CurrentWeapon->WeaponInfo.NameItem));
			newind = ind - 1;
			if (CurrentWeapon->WeaponReloading)
			{
				CurrentWeapon->CancelReload();
				GetMesh()->GetAnimInstance()->StopAllMontages(0.015f);
				if (CurrentWeapon->WeaponSetting.DropMagaz)
				{
					dropmag->Destroy();
				}
				if (CurrentWeapon->WeaponSetting.NewMagaz)
				{
					newmag->Destroy();
				}
			}
		}
		if (InventoryComponent)
		{
			InventoryComponent->SwitchWeaonToIndex(newind, ind, OldInfo,false,false);
		}		
	}
}

void ATDSCharacter::Sprinting()
{
	SpeedRunEnabled = true;
	ChangeMovementState();
}


void ATDSCharacter::Sprinting_Done()
{
	SpeedRunEnabled = false;
	ChangeMovementState();
}

void ATDSCharacter::Walking()
{
	WalkEnabled = true;
	ChangeMovementState();
}

void ATDSCharacter::Walking_Done()
{
	WalkEnabled = false;
	ChangeMovementState();
}

void ATDSCharacter::Aiming()
{
	AimEnabled = true;
	ChangeMovementState();
}

void ATDSCharacter::Aiming_Done()
{
	AimEnabled = false;
	ChangeMovementState();
}

void ATDSCharacter::LeftCLicked()
{
	if (!SpeedRunEnabled)
	{
		if (((!WalkEnabled && !SpeedRunEnabled && !AimEnabled && !SittingEnabled && !LieingEnabled) && ((AxisX != 0) || (AxisY != 0))))
		{
			AttackCharEvent(false);
		}
		else
		{
			AttackCharEvent(true);
		}
	}
	else
	{
		AttackCharEvent(false);
	}
	LeftButton = true;
	ChangeMovementState();
}

void ATDSCharacter::LeftReleased()
{
	LeftButton = false;
	AttackCharEvent(false);
	ChangeMovementState();
}

void ATDSCharacter::Sitting()
{
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetIgnoreMoveInput(true);
	if (LieingEnabled)
	{
		LieingEnabled = false;
		AimEnabled = false;
	}
	if (SittingEnabled)
	{
		SittingEnabled = false;
		AimEnabled = false;
		ChangeMovementState();
		PlayAnimMontage(CrouchToStanMontage);
	}
	else
	{
		SittingEnabled = true;
		ChangeMovementState();
		PlayAnimMontage(StandToCrouchMontage);
	}
}

void ATDSCharacter::Liing()
{
	
	UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetIgnoreMoveInput(true);
	if (SittingEnabled)
	{
		SittingEnabled = false;
		AimEnabled = false;
	}
	if (LieingEnabled)
	{
		LieingEnabled = false;
		AimEnabled = false;
		ChangeMovementState();
		PlayAnimMontage(ProneToStandMontage);
	}
	else
	{
		LieingEnabled = true;
		ChangeMovementState();
		PlayAnimMontage(StandToProneCrouchMontage);
	}

}

void ATDSCharacter::Jumping()
{
	Jump();
	UGameplayStatics::PlaySound2D(GetWorld(), SoundJump);

}

void ATDSCharacter::InitGranate()
{
	StandartWeaponName = CurrentWeapon->WeaponInfo.NameItem;
	int32 ind = InventoryComponent->GetWeaponIndexSlotByName(InventoryComponent->GetWeaponNameSlotByType(EWeaponType::Grenate));
	int32 oldind = InventoryComponent->GetWeaponIndexSlotByName(FName(CurrentWeapon->WeaponInfo.NameItem));
	InventoryComponent->SwitchWeaonToIndex(ind, oldind, CurrentWeapon->WeaponInfo,true,true);
	ReloadWeapon();
	//InitWeapon(InventoryComponent->WeaponSlots[ind].NameItem, InventoryComponent->WeaponSlots[ind]);
}

void ATDSCharacter::ThrowGran()
{
	if (CurrentWeapon->WeaponInfo.Ammo.Count)
	{
		CurrentWeapon->Fire();
	}

	int32 ind = InventoryComponent->GetWeaponIndexSlotByName(FName(StandartWeaponName));
	int32 oldind = InventoryComponent->GetWeaponIndexSlotByName(FName(CurrentWeapon->WeaponInfo.NameItem));
	InventoryComponent->SwitchWeaonToIndex(ind, oldind, CurrentWeapon->WeaponInfo,true,false);
	//InitWeapon(InventoryComponent->WeaponSlots[ind].NameItem, InventoryComponent->WeaponSlots[ind]);
}

void ATDSCharacter::ShootMontage()
{
	InventoryComponent->OnUpdateWidget.Broadcast(CurrentWeapon->WeaponInfo,false);
	InventoryComponent->SetAdditionalWeaponInfo(InventoryComponent->GetWeaponIndexSlotByName(CurrentWeapon->WeaponInfo.NameItem), CurrentWeapon->WeaponInfo);
	if (!LieingEnabled)
	{
			if (AimEnabled)
			{
				CurrentWeapon->Droping();
				PlayAnimMontage(CurrentWeapon->WeaponSetting.AnimAimCharFire);		
			}
			else
			{
				CurrentWeapon->Droping();
				PlayAnimMontage(CurrentWeapon->WeaponSetting.AnimCharFire);
			}
	}

}

void ATDSCharacter::OnCompletedMontage(UAnimMontage* aMontage, bool bInterrupted)
{
	if ((aMontage == CrouchToStanMontage) || (aMontage == StandToCrouchMontage) || (aMontage == ProneToStandMontage) || (aMontage == StandToProneCrouchMontage))
	{
		UGameplayStatics::GetPlayerController(GetWorld(), 0)->SetIgnoreMoveInput(false);
	}
	if (aMontage == CurrentWeapon->WeaponSetting.AnimCharReload)
	{
		if (CurrentWeapon->WeaponSetting.DropMagaz)
		{
			dropmag->Destroy();
		}
		if (CurrentWeapon->WeaponSetting.NewMagaz)
		{
			newmag->Destroy();
		}
	}
}




