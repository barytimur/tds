// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDSInventoryComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FOnSwitchWeapon, FName, WeaponIdName, FWeaponSlot, AddicionalWeaponInfo, bool, bIsOld, bool, bIsSwap);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateWidget, FWeaponSlot, InfoAboutBullets, bool, Swap);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCreateeSLot, FWeaponSlot, NewWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSwitchWhenSwap, FName, NewWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwapWeapon, FWeaponInfo, WeaponInfoToSwap, int32, index, FWeaponSlot, Ammonition);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UTDSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTDSInventoryComponent();

	FOnSwitchWeapon OnSwitchWeapon;
	UPROPERTY(BlueprintAssignable)
	FOnUpdateWidget OnUpdateWidget;
	UPROPERTY(BlueprintAssignable)
	FCreateeSLot CreateSlot;
	UPROPERTY(BlueprintAssignable)
	FOnSwapWeapon OnSwapWeapon;
	UPROPERTY(BlueprintAssignable)
	FOnSwitchWhenSwap OnSwitchWhenSwap;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FWeaponSlot> WeaponSlots;
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FArmySlot> ArmySlots;*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		int32 MaxSlots = 0;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	bool SwitchWeaonToIndex(int32 NewIndex, int32 oldIndex, FWeaponSlot oldInfo, bool bIsNext, bool bIsGranate);

	FWeaponSlot GetAddicionalWeaponInfo(int32 indexWeapon);
	UFUNCTION(BlueprintCallable)
	int32 GetWeaponIndexSlotByName(FName IdWeaponName);
	FName GetWeaponNameSlotByType(EWeaponType IdWeaponType);
	void SetAdditionalWeaponInfo(int32 IndexWeapon, FWeaponSlot NewInfo);
	UFUNCTION(BlueprintCallable)
	bool AddCurrentBullets(EWeaponType WeaponType, int32 CounterBullets, EWeaponType CurWeaponType);
	int32 CheckSlots();
	UFUNCTION(BlueprintCallable)
	bool PickUpWeapon(FWeaponSlot WeaponSlot);
	UFUNCTION(BlueprintCallable)
	FWeaponSlot SwapWeapon(FWeaponSlot NewWeaponSlot, FWeaponSlot CurrentWeaponSlot);
};
